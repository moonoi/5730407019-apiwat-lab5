package wow1;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Text;

public class CreateXMLDoc {
	public static void main(String[] args) {
		try {
			String xmlProfile = "myProfile.xml";
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element root = doc.createElement("profile");
			Element nameE = doc.createElement("name");
			Text nameT = doc.createTextNode("Apiwat Kitchanukit");
			nameE.appendChild(nameT);
			root.appendChild(nameE);
			doc.appendChild(root);

			TransformerFactory transformer = TransformerFactory.newInstance();
			Transformer trans = transformer.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("D:\\xmlfile\\"
					+ xmlProfile));

			trans.transform(source, result);
			System.out.println("File save!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
}
